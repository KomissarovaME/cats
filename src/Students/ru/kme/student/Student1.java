package Students.ru.kme.student;
import Students.ru.kme.interfaces.English;
import Students.ru.kme.interfaces.German;

public class Student1 implements English, German{

    @Override
    public void languageEnglish() {
        System.out.println("Изучает Английский язык.");
    }

    @Override
    public void languageGerman() {
        System.out.println("Изучает Немецкий язык.");
    }
}
