package Shapes;

public class Figure {
    private Colour colour;
    private double area;

    private Figure(Colour colour, double area){
        this.area = area;
        this.colour = colour;
    }

}
