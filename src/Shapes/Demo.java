package Shapes;

public class Demo {
    public static void main (String()args){
        Circle circle = new Circle(Colour.BLACK new Point(), 5);
        Traingle traingle = new Traingle(Colour.RED, new Point(0, 0), new Point(1, 0));
        Square square = new Square(Colour.WHITE, new Point(1,1), 5);

        //Пример преобразования типов объекта.
        Shape shape = traingle;
        Object o = traingle;
        Traingle traingle1 = (Traingle) o;

        Shape[] shapes = {circle, traingle, square};
        printArrayElements(shape);

        Shape maxShape = maxShapeArea(shapes);
        System.out.println("Фигура с наибольшей площадью" + maxShape);
    }
    private static void printArrayElements(Object[] objects){
        for (Object object : objects){
            System.out.println(object + "");
        }
    }
    private static Shape maxShapeArea(Shape[] shapes){
        Shape maxShape = null;
        double maxArea = Double.NEGATIVE_INFINITY;//отрицаткльная бесконечность.
        for (Shape shape : shapes){
            double area = shape.area();
            if(area > maxArea){
                maxShape = shape;
                maxArea = area;
            }
        }
        return maxShape;
    }
}
