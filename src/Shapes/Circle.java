
package Shapes;

public class Circle {
    private Point point;
    private double radius;
    private Colour colour;

    private Circle(Point point, double radius){
        this.point = point;
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "point=" + point +
                ", radius=" + radius +
                ", colour=" + colour +
                '}';
    }

    Circle(Colour colour, Point center, double radius){
        super(colour);

    }
}
