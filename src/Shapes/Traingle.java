package Shapes;

public class Traingle {
    private Point a;
    private Point b;
    private Point c;

    public Traingle(Colour colour, Point a, Point b, Point c){
        super(colour);
        this.a = a;
        this.b = b;
        this.c = c;
    }
    public double area(){
        return Math.abs((a.getX() - c.getX()) * (b.getY() - c.getY() - (b.getX() - c.getX()) * (a.getY() - c.getY())) / 2;
    }

    @Override
    public String toString() {
        return "Traingle{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }
}
