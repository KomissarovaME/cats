package Shapes;

public abstract class Shape {
    private Colour colour;

    public Shape(Colour color){
        this.colour = color;
    }
    public Colour getColor(){
        return colour;
    }
    public abstract double area();
}

