package Cats;

public class Cat {
    /**
     * @param gender пол
     * @param name имя
     * @param year возраст
     * @param breed порода
     */
    private char gender;
    private String name;
    private int year;
    private String breed;

    public Cat(char gender, String name, int year, String breed) {
        this.gender = gender;
        this.name = name;
        this.year = year;
        this.breed = breed;
    }

    public String getName() {
        return name;
    }
    public char getGender() {
        return gender;
    }
    public int getYear() {
        return year;
    }
    public String getBreed() {
        return breed;
    }

    public void omnom(){ System.out.println("Ням ням ням."); }
    public void voice(){
        System.out.println("Мур мур мур.");
    }

    @Override
    public String toString() {
        return "Cat{" +
                "пол" + gender +
                ", имя'" + name + '\'' +
                ", год" + year +
                ", порода'" + breed + '\'' +
                '}';
    }
}
