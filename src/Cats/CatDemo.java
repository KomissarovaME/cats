package Cats;

public class CatDemo {

    public static void main(String[] args) {
        Cat cat1 = new Cat('ж', "Маша", 2016, "Усатый-полосатый");
        Cat cat2 = new Cat('ж', "Наташа", 2008, "Усатый-безполосый");
        Cat cat3 = new Cat('м', "Саша", 2012, "Усатый-неполосатый");
        Cat[] cats = {cat1,cat2,cat3};
        System.out.println(cats);
        old(cats);
    }
    public static void old(Cat[] cats){
        Cat oldest = cats[0];
        for(int i = 1; i < cats.length; i++) {
            if(oldest.getYear() > cats[i].getYear()) {
                oldest = cats[i] ;
                System.out.println("Самый старый кошак " + oldest);
            }
        }
    }
}
