package Robots;

import static Robots.Direction.up;

import java.util.Scanner;

public class MainRobot {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Robot robotInitial = new Robot(0, 0, up);
        Scanner reader = new Scanner(System.in);
        System.out.println("Введите координаты конечной точки пути робота.");
        int x = reader.nextInt();
        int y = reader.nextInt();
        Robot robotFinal = new Robot(x, y, up);


    }
}